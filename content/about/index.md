---
title: "About me"
#date: 2020-09-15T11:30:03+00:00
# weight: 1
# aliases: ["/first"]
#tags: ["first"]
#author: "Edward De Brouwer"
# author: ["Me", "You"] # multiple authors
showToc: true
TocOpen: false
draft: false
hidemeta: true
comments: false
#description: "Desc Text."
canonicalURL: "https://canonical.url/to/page"
disableHLJS: false # to disable highlightjs
disableShare: true
disableHLJS: false
hideSummary: false
searchHidden: true
ShowReadingTime: false
ShowBreadCrumbs: false
ShowPostNavLinks: false
cover:
    image: "images/Eddy.jpg" # image path/url
    alt: "<alt text>" # alt text
    caption: "Me" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://github.com/<path_to_repo>/content"
    Text: "Suggest Changes" # edit text
    appendFilePath: true # to append file path to Edit link
---

{{< figure src="Eddy.jpg#center" alt="Edward" width="300px" >}}


I'm Edward, a last-year Machine Learning PhD student and Philosophy student at KU Leuven, Belgium.

My machine learning interests revolve around **time series** and **graphs** with applications in **healthcare**. My main focus is on developing robust and interpretable methods that can improve the cooperation between humans and AI. I'm studying this interaction from a computational and philosophical perspective.

You can download a full CV [here](cv.pdf)
